core = 7.x
api = 2

; Contrib Modules
projects[boxes][version] = "1.0"
projects[boxes][subdir] = "contrib"

projects[ctools][version] = "1.2"
projects[ctools][subdir] = "contrib"

projects[date][version] = "2.2"
projects[date][subdir] = "contrib"

projects[deploy][type] = "module"
projects[deploy][subdir] = "contrib"
projects[deploy][download][type] = "git"
projects[deploy][download][revision] = "63ea46d07146b5cef5d309fc9989b5b015198b95"
projects[deploy][download][branch] = "2.x-dev"

projects[entity][version] = "1.0-rc3"
projects[entity][subdir] = "contrib"


projects[entity_dependency][type] = "module"
projects[entity_dependency][subdir] = "contrib"
projects[entity_dependency][download][type] = "git"
projects[entity_dependency][download][revision] = "7ca711a5149083ca2aa3b6ada9d35cd5abc6c99a"
projects[entity_dependency][download][branch] = "7.x-1.x"

projects[features][version] = "1.0"
projects[features][subdir] = "contrib"

projects[flexslider][version] = "1.0-rc3"
projects[flexslider][subdir] = "contrib"

projects[file_entity][version] = "2.0-unstable6"
projects[file_entity][subdir] = "contrib"

projects[globalredirect][version] = "1.5"
projects[globalredirect][subdir] = "contrib"


projects[libraries][version] = "1.0"
projects[libraries][subdir] = "contrib"

projects[link][version] = "1.0"
projects[link][subdir] = "contrib"

projects[media][version] = "2.0-unstable6"
projects[media][subdir] = "contrib"

projects[navigation404][version] = "1.0"
projects[navigation404][subdir] = "contrib"

projects[pathauto][version] = "1.2"
projects[pathauto][subdir] = "contrib"


projects[resp_img][version] = "1.3"
projects[resp_img][subdir] = "contrib"

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = "contrib"

projects[token][version] = "1.4"
projects[token][subdir] = "contrib"

projects[typekit][version] = "1.0-beta1"
projects[typekit][subdir] = "contrib"

projects[uuid][type] = "module"
projects[uuid][subdir] = "contrib"
projects[uuid][download][type] = "git"
projects[uuid][download][revision] = "4730c678a5a09ab0529ea3222e6123bd929b8da0"
projects[uuid][download][branch] = "7.x-1.x"

projects[views][version] = "3.5"
projects[views][subdir] = "contrib"
  
projects[views_slideshow][version] = "3.0"
projects[views_slideshow][subdir] = "contrib"

; Themes
projects[golden_grid][type] = "theme"
projects[golden_grid][download][type] = "git"
projects[golden_grid][download][revision] = "9190aa8a075d9dd2f715c66c4cf850ab8d0745d3"
projects[golden_grid][download][branch] = "7.x-1.x"

; Libraries
; libraries[flexslider][download][type] = "get"
; libraries[flexslider][download][url] = "https://github.com/woothemes/FlexSlider/zipball/flexslider1"
; libraries[flexslider][directory_name] = "flexslider"
; libraries[flexslider][type] = "library"

libraries[profiler][download][type] = "get"
libraries[profiler][download][url] = "http://ftp.drupal.org/files/projects/profiler-7.x-2.0-beta1.tar.gz"
libraries[profiler][directory_name] = "profiler"
libraries[profiler][type] = "library"

; Development

projects[devel][version] = "1.2"
projects[devel][subdir] = "devel"

projects[devel_themer][type] = "module"
projects[devel_themer][subdir] = "devel"
projects[devel_themer][download][type] = "git"
projects[devel_themer][download][revision] = "ea80046f0eb86a19727f50e3a96c95543f01edb2"
projects[devel_themer][download][branch] = "7.x-1.x"

projects[diff][version] = "2.0"
projects[diff][subdir] = "devel"

projects[golden_gridlet][type] = "module"
projects[golden_gridlet][subdir] = "devel"
projects[golden_gridlet][download][type] = "git"
projects[golden_gridlet][download][revision] = "ba946be5b6e5f32392f8a85664c03d560b41760e"
projects[golden_gridlet][download][branch] = "7.x-1.x"

